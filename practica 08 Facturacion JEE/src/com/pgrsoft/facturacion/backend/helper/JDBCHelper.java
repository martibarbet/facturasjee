package com.pgrsoft.facturacion.backend.helper;

import java.sql.Connection;
import java.sql.DriverManager;

public abstract class JDBCHelper {
	
	private static Connection CON = null;

	static {
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			
			CON = DriverManager.getConnection("jdbc:mysql://10.250.0.8/schemaa?" +
			        "user=user04&password=1111");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	public static Connection getConnection() {
		return CON;
	}
}
