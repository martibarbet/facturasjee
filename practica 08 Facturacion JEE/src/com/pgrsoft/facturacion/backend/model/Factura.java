package com.pgrsoft.facturacion.backend.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Factura {
	
	private int codigo;
	private Date fecha;
	private Cliente cliente;
	private List<LineaFactura> detalle = new ArrayList<LineaFactura>();

	public Factura() {
		
	}
	
	public Factura(int codigo, Date fecha, Cliente cliente) {
		this.codigo = codigo;
		this.fecha = fecha;
		this.cliente = cliente;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<LineaFactura> getDetalle() {
		return detalle;
	}

	public void addLineaFactura(Producto producto, int cantidad, double precio) {
		LineaFactura lineaFactura = new LineaFactura(producto, cantidad, precio);
		this.detalle.add(lineaFactura);
	}
	
	public double getBaseImponible() {
		
		double baseImponible = 0.0;
		
		for (LineaFactura lineaFactura: this.detalle) {
			baseImponible += lineaFactura.getTotal();
		}
		
		return baseImponible;
	}
	
	public double getIva() {
		return this.getBaseImponible() * 0.21;
	}
	
	public double getTotalFactura() {
		double baseImponible = this.getBaseImponible();
		return baseImponible + (baseImponible * 0.21);
	}

	@Override
	public String toString() {
		return "Factura [codigo=" + codigo + ", fecha=" + fecha + ", cliente=" + cliente + ", detalle=" + detalle + "]";
	}
	
	
}
