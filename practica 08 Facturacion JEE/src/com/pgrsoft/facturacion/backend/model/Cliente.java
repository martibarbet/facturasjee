package com.pgrsoft.facturacion.backend.model;

public class Cliente {

	private String CIF;
	private String nombre;
	private String direccion;
	private String poblacion;
	private String codigoPostal;
	private String provincia;
	private String pais;
	
	public Cliente() {
		
	}
	
	public Cliente(String CIF, String nombre) {
		this.CIF = CIF;
		this.nombre = nombre;
	}

	public Cliente(String cIF, String nombre, String direccion, String poblacion, String codigoPostal, String provincia, String pais) {
		CIF = cIF;
		this.nombre = nombre;
		this.direccion = direccion;
		this.poblacion = poblacion;
		this.codigoPostal = codigoPostal;
		this.provincia = provincia;
		this.pais = pais;
	}

	public String getCIF() {
		return CIF;
	}

	public void setCIF(String cIF) {
		CIF = cIF;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getPoblacion() {
		return poblacion;
	}

	public void setPoblacion(String poblacion) {
		this.poblacion = poblacion;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((CIF == null) ? 0 : CIF.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cliente other = (Cliente) obj;
		if (CIF == null) {
			if (other.CIF != null)
				return false;
		} else if (!CIF.equals(other.CIF))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Cliente [CIF=" + CIF + ", nombre=" + nombre + ", direccion=" + direccion + ", poblacion=" + poblacion
				+ ", codigoPostal=" + codigoPostal + ", provincia=" + provincia + ", pais=" + pais + "]";
	}

}
