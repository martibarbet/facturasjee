package com.pgrsoft.facturacion.backend.services;

import java.util.List;

import com.pgrsoft.facturacion.backend.model.Producto;

public interface ProductoServices {

	// Operaciones CRUD
	
	public void create(Producto producto) throws Exception;
	public Producto read(int codigo) throws Exception;
	public void update(Producto producto) throws Exception;
	
	// Obtenci�n de listas
	public List<Producto> getAll() throws Exception;
	
	/**
	 * 
	 * @param min (included)
	 * @param max (included)
	 * @return
	 */
	public List<Producto> getBetweenPrices(double min, double max) throws Exception;
	
}
