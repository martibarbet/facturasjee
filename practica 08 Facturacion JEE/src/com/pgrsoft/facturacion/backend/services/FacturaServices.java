package com.pgrsoft.facturacion.backend.services;

import java.util.Date;
import java.util.List;

import com.pgrsoft.facturacion.backend.model.Cliente;
import com.pgrsoft.facturacion.backend.model.Factura;

public interface FacturaServices {

	// Operaciones CRUD
	public void create(Factura factura) throws Exception;
	public Factura read(int codigo) throws Exception;
	
	// Obtenci�n de listas
	public List<Factura> getAll() throws Exception;
	public List<Factura> getByCliente(Cliente cliente) throws Exception;
	
	/**
	 * 
	 * @param desde (included)
	 * @param hasta (included)
	 * @return
	 */
	public List<Factura> getBetweenDates(Date desde, Date hasta) throws Exception;
}
