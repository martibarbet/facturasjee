package com.pgrsoft.facturacion.backend.services;

import java.util.List;

import com.pgrsoft.facturacion.backend.model.Cliente;

public interface ClienteServices {

	// Operaciones CRUD
	
	public void create(Cliente cliente) throws Exception;
	public Cliente read(String CIF) throws Exception;
	
	// Obtenci�n de listas
	
	public List<Cliente> getAll() throws Exception;
	
	
}
