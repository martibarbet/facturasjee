package com.pgrsoft.facturacion.backend.services.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.pgrsoft.facturacion.backend.helper.JDBCHelper;
import com.pgrsoft.facturacion.backend.model.Producto;
import com.pgrsoft.facturacion.backend.services.ProductoServices;

public class ProductoServicesImpl implements ProductoServices {
	
	private Connection con = JDBCHelper.getConnection(); 
	
	@Override
	public void create(Producto producto) throws Exception{
		String strSQL = "INSERT INTO PRODUCTOS (CODIGO, NOMBRE, PRECIO) VALUES (?,?,?)";
		
		PreparedStatement ps = this.con.prepareStatement(strSQL);
		ps.setInt(1, producto.getCodigo());
		ps.setString(2, producto.getNombre());
		ps.setDouble(3, producto.getPrecio());
				
		System.out.println("He introducido un Producto: " + ps.execute());
	}

	@Override
	public Producto read(int codigo) throws Exception {
		Producto producto = new Producto(); 
		String strSQL = "SELECT CODIGO, NOMBRE, PRECIO FROM PRODUCTOS WHERE CODIGO = ?;";

		PreparedStatement ps = this.con.prepareStatement(strSQL);
		ps.setInt(1, codigo);
		ResultSet rs = ps.executeQuery();
			
		if(rs.next())
		{
			producto =new Producto(rs.getInt(1), rs.getString(2), rs.getDouble(3));
		}
		
		return producto;
	}

	@Override
	public void update(Producto producto) throws Exception {
		String strSQL = "UPDATE PRODUCTOS SET NOMBRE = ?, PRECIO = ? WHERE CODIGO = ?;";
		
		PreparedStatement ps = this.con.prepareStatement(strSQL);
		ps.setString(1, producto.getNombre());
		ps.setDouble(2, producto.getPrecio());
		ps.setInt(3, producto.getCodigo());

		ps.execute();
	}

	@Override
	public List<Producto> getAll() throws Exception {
		List<Producto> productos = new ArrayList<Producto>(); 
		String strSQL = "SELECT CODIGO, NOMBRE, PRECIO FROM PRODUCTOS;";
		
		PreparedStatement ps = this.con.prepareStatement(strSQL);
		ResultSet rs = ps.executeQuery();
				
		while(rs.next())
		{
			productos.add(new Producto(rs.getInt(1), rs.getString(2), rs.getDouble(3)));
		}
		
		return productos;
	}

	@Override
	public List<Producto> getBetweenPrices(double min, double max) throws Exception{
		List<Producto> productos = new ArrayList<Producto>(); 
		String strSQL = "SELECT CODIGO, NOMBRE, PRECIO FROM PRODUCTOS WHERE PRECIO BETWEEN ? AND ?;";
		
		PreparedStatement ps = this.con.prepareStatement(strSQL);
		ps.setDouble(1, min);
		ps.setDouble(2, max);
		ResultSet rs = ps.executeQuery();
				
		while(rs.next())
		{
			productos.add(new Producto(rs.getInt(1), rs.getString(2), rs.getDouble(3)));
		}
		
		return productos;
	}

}
