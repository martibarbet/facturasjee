package com.pgrsoft.facturacion.backend.services.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pgrsoft.facturacion.backend.helper.JDBCHelper;
import com.pgrsoft.facturacion.backend.model.Cliente;
import com.pgrsoft.facturacion.backend.model.Factura;
import com.pgrsoft.facturacion.backend.model.LineaFactura;
import com.pgrsoft.facturacion.backend.model.Producto;
import com.pgrsoft.facturacion.backend.services.ClienteServices;
import com.pgrsoft.facturacion.backend.services.FacturaServices;
import com.pgrsoft.facturacion.backend.services.ProductoServices;

public class FacturaServicesImpl implements FacturaServices {

	private Connection con = JDBCHelper.getConnection(); 
	
	private ClienteServices clienteServices = new ClienteServicesImpl();
	private ProductoServices productoServices = new ProductoServicesImpl();
	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	@Override
	public void create(Factura factura) throws Exception {
		
		String strCreateFactura = "INSERT INTO FACTURAS (CODIGO, FECHA, ID_CLIENTE) VALUES (?,?,?) ";
		String strInsertLineaF = "INSERT INTO LINEAS_FACTURA (id_factura, id_producto, cantidad, precio) VALUES (?,?,?,?)";
		
		con.setAutoCommit(false);
		
		try {
			PreparedStatement ps = this.con.prepareStatement(strCreateFactura);
			ps.setInt(1, factura.getCodigo());
			ps.setString(2, sdf.format(factura.getFecha()));
			ps.setString(3, factura.getCliente().getCIF());
			
			ps.execute();
				
			//int a = 10/0;
			
			PreparedStatement ps2 = this.con.prepareStatement(strInsertLineaF);
			for(LineaFactura linea : factura.getDetalle())
			{
				ps2.setInt(1, factura.getCodigo());
				ps2.setInt(2, linea.getProducto().getCodigo());
				ps2.setInt(3, linea.getCantidad());
				ps2.setDouble(4, linea.getPrecio());
				ps2.execute();
			}
			
			con.commit();
		}finally {
			con.setAutoCommit(true);
		}
	}

	@Override
	public Factura read(int codigo) throws Exception{
		Factura factura = new Factura();
		
		String strFacturaSQL = "SELECT CODIGO, FECHA, ID_CLIENTE FROM FACTURAS WHERE CODIGO = ?;";
		String strLineaSQL = "SELECT ID_PRODUCTO, CANTIDAD, PRECIO FROM LINEAS_FACTURA WHERE ID_FACTURA = ?";
		
		Cliente tempClient = null;
		Producto tempProd = null;
				
		PreparedStatement ps = this.con.prepareStatement(strFacturaSQL);
		ps.setInt(1, codigo);
		ResultSet rs = ps.executeQuery();
				
		PreparedStatement ps2 = this.con.prepareStatement(strLineaSQL);
		ResultSet rs2 = null;
		if(rs.next())
		{
			tempClient = clienteServices.read(rs.getString(3));
			factura = new Factura(rs.getInt(1), rs.getDate(2), tempClient);
					
			ps2.setInt(1, rs.getInt(1));
			rs2 = ps2.executeQuery();
			while(rs2.next())
			{
				tempProd = productoServices.read(rs2.getInt(1));
				factura.addLineaFactura(tempProd,  rs2.getInt(2), rs2.getDouble(3));
			}
		}
		
		return factura;
	}

	@Override
	public List<Factura> getAll() throws Exception{
		
		String consultaFacturas = "SELECT * FROM FACTURAS JOIN LINEAS_FACTURA ON FACTURAS.CODIGO = LINEAS_FACTURA.ID_FACTURA";
	
		Map<Integer, Factura> mapFacturas = new HashMap<Integer, Factura>();
		
		PreparedStatement preparedStatement = con.prepareStatement(consultaFacturas);
		ResultSet resultSet = preparedStatement.executeQuery();
		
		while (resultSet.next()) {
			
			int codigoFactura = resultSet.getInt(1);
			
			if(!mapFacturas.containsKey(codigoFactura)) {
				mapFacturas.put(codigoFactura, new Factura(codigoFactura, 
														   resultSet.getDate(2), 
														   clienteServices.read(resultSet.getString(3)))
															);
			} 
			
			mapFacturas.get(codigoFactura).addLineaFactura(productoServices.read(resultSet.getInt(6)),
																			   	 resultSet.getInt(7), 
																			   	 resultSet.getDouble(8)
																			   	 );
		}
		
		return new ArrayList<Factura>(mapFacturas.values());
		
	}

	@Override
	public List<Factura> getByCliente(Cliente cliente) throws Exception{
		
		String consultaFacturas = "SELECT * FROM FACTURAS JOIN LINEAS_FACTURA ON FACTURAS.CODIGO = LINEAS_FACTURA.ID_FACTURA WHERE ID_CLIENTE = ?";
		
		Map<Integer, Factura> mapFacturas = new HashMap<Integer, Factura>();
		
		PreparedStatement preparedStatement = con.prepareStatement(consultaFacturas);
		preparedStatement.setString(1, cliente.getCIF());
		ResultSet resultSet = preparedStatement.executeQuery();
		
		while (resultSet.next()) {
			
			int codigoFactura = resultSet.getInt(1);
			
			if(!mapFacturas.containsKey(codigoFactura)) {
				mapFacturas.put(codigoFactura, new Factura(codigoFactura, 
														   resultSet.getDate(2), 
														   clienteServices.read(resultSet.getString(3))));
			} 
			
			mapFacturas.get(codigoFactura).addLineaFactura(productoServices.read(resultSet.getInt(6)),
					   resultSet.getInt(7), 
					   resultSet.getDouble(8));
		}
		
		return new ArrayList<Factura>(mapFacturas.values());
	}

	@Override
	public List<Factura> getBetweenDates(Date desde, Date hasta) throws Exception {
		
		String consultaFacturas = "SELECT * FROM FACTURAS JOIN LINEAS_FACTURA ON FACTURAS.CODIGO = LINEAS_FACTURA.ID_FACTURA WHERE FECHA BETWEEN ? AND ?";
		
		Map<Integer, Factura> mapFacturas = new HashMap<Integer, Factura>();
		
		PreparedStatement preparedStatement = con.prepareStatement(consultaFacturas);
		preparedStatement.setString(1, sdf.format(desde));
		preparedStatement.setString(2, sdf.format(hasta));
		ResultSet resultSet = preparedStatement.executeQuery();
		
		while (resultSet.next()) {
			
			int codigoFactura = resultSet.getInt(1);
			
			if(!mapFacturas.containsKey(codigoFactura)) {
				mapFacturas.put(codigoFactura, new Factura(codigoFactura, 
														   resultSet.getDate(2), 
														   clienteServices.read(resultSet.getString(3))));
			} 
			
			mapFacturas.get(codigoFactura).addLineaFactura(productoServices.read(resultSet.getInt(6)),
					   resultSet.getInt(7), 
					   resultSet.getDouble(8));
		}
		
		return new ArrayList<Factura>(mapFacturas.values());
		
	}

}
