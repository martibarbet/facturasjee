package com.pgrsoft.facturacion.backend.services.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.pgrsoft.facturacion.backend.helper.JDBCHelper;
import com.pgrsoft.facturacion.backend.model.Cliente;
import com.pgrsoft.facturacion.backend.services.ClienteServices;

public class ClienteServicesImpl implements ClienteServices {

	private Connection con = JDBCHelper.getConnection(); 
	
	@Override
	public void create(Cliente cliente) throws Exception{
		
		String strSQL = "INSERT INTO CLIENTES (CIF, NOMBRE, DIRECCION, POBLACION, CODIGO_POSTAL, PROVINCIA, PAIS) VALUES (?,?,?,?,?,?,?)";
		
		PreparedStatement ps = this.con.prepareStatement(strSQL);
		
		ps.setString(1, cliente.getCIF());
		ps.setString(2, cliente.getNombre());
		ps.setString(3, cliente.getDireccion());
		ps.setString(4, cliente.getPoblacion());
		ps.setString(5, cliente.getCodigoPostal());
		ps.setString(6, cliente.getProvincia());
		ps.setString(7, cliente.getPais());		
		
		ps.execute();
	}

	@Override
	public Cliente read(String CIF) throws Exception {
		
		Cliente cliente = null; 
		
		String strSQL = "SELECT CIF, NOMBRE, DIRECCION, POBLACION, CODIGO_POSTAL, PROVINCIA, PAIS FROM CLIENTES WHERE CIF = ?;";
		
		PreparedStatement ps = this.con.prepareStatement(strSQL);
		ps.setString(1, CIF);
				
		ResultSet rs = ps.executeQuery();
				
		if(rs.next())
		{
			cliente = new Cliente(rs.getString(1), rs.getString(2), rs.getString(3),  rs.getString(4),  rs.getString(7),  rs.getString(5),  rs.getString(6));
		}
		
		return cliente;
	}

	@Override
	public List<Cliente> getAll() throws Exception {
		
		List<Cliente> clientes = new ArrayList<Cliente>(); 
		
		String strSQL = "SELECT CIF, NOMBRE, DIRECCION, POBLACION, CODIGO_POSTAL, PROVINCIA, PAIS FROM CLIENTES;";
		
		PreparedStatement ps = this.con.prepareStatement(strSQL);
		ResultSet rs = ps.executeQuery();
				
		while(rs.next())
		{
			clientes.add(new Cliente(rs.getString(1), 
									 rs.getString(2), 
									 rs.getString(3),  
									 rs.getString(4), 
									 rs.getString(5),  
									 rs.getString(6), 
									 rs.getString(7)));
		}
		
		return clientes;
	}

}
