package com.pgrsoft.facturacion.backend.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pgrsoft.facturacion.backend.model.Producto;
import com.pgrsoft.facturacion.backend.services.ProductoServices;
import com.pgrsoft.facturacion.backend.services.impl.ProductoServicesImpl;

@WebServlet("/testproducto")
public class SvTestProducto extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private ProductoServices productoServices = new ProductoServicesImpl();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		testCreate();
		testGetAll();
		testBetweenPrices();
		
		response.sendRedirect("index.jsp");
	}
	
	private void testCreate()
	{
		System.out.println("\n************ Test create() **************************\n");
		
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 5; i++)
		{
			sb.append((int)(Math.random() * 10));
		}
		
		Producto producto = new Producto(Integer.parseInt(sb.toString()), "Tuercas de colores" ,(Math.random()*10)*(Math.random() * 10));
		
		try {
			this.productoServices.create(producto);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private List<Producto> testGetAll()
	{
		System.out.println("\n************ Test getAll() **************************\n");
		List<Producto> productos = null;
		try {
			productos = productoServices.getAll();
		
			for(Producto producto: productos)
			{
				System.out.println(producto);
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return productos;
	}

	private List<Producto> testBetweenPrices()
	{
		System.out.println("\n************ Test getBetweenPrices() **************************\n");
		
		List<Producto> productos = null;
		
		try {
			productos = productoServices.getBetweenPrices(100, 500);
			
			for(Producto producto: productos)
			{
				System.out.println(producto);
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return productos;
	}
}
