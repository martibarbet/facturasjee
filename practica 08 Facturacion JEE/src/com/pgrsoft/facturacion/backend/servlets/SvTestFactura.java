package com.pgrsoft.facturacion.backend.servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pgrsoft.facturacion.backend.model.Cliente;
import com.pgrsoft.facturacion.backend.model.Factura;
import com.pgrsoft.facturacion.backend.services.ClienteServices;
import com.pgrsoft.facturacion.backend.services.FacturaServices;
import com.pgrsoft.facturacion.backend.services.impl.ClienteServicesImpl;
import com.pgrsoft.facturacion.backend.services.impl.FacturaServicesImpl;


@WebServlet("/testfactura")
public class SvTestFactura extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private FacturaServices facturaServices = new FacturaServicesImpl();
	private ClienteServices clienteServices = new ClienteServicesImpl();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		testRead(14000);
		
		testCreate();
		
		testGetAll();
		
		
			Cliente cliente = null;
			try {
				cliente = clienteServices.read("37655982K");
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		
		testGetByCliente();
		
		
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			Date desde = null;
			Date hasta = null;
			try {
				desde = sdf.parse("01-01-2018");
				hasta = sdf.parse("29-01-2018");
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
		testGetBetweenDates(desde, hasta);
		
		response.sendRedirect("index.jsp");
	}
	
	private Factura testRead(int codigo)
	{
		System.out.println("\n************ TestRead(codigo) **************************\n");
		Factura factura = null;
		try {
			factura = facturaServices.read(14000);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return factura;
	}
	
	private void testCreate()
	{
		System.out.println("\n************ TestCreate(factura) **************************\n");
		
		Factura factura = new Factura();
		
		StringBuilder codiSB = new StringBuilder();
		for(int i=0; i<4; i++)
		{
			codiSB.append(i);
		}
		
		try {
			factura.setCodigo(Integer.parseInt(codiSB.toString()));
			factura.setFecha(new Date());
			factura.setCliente(clienteServices.read("37655928K"));
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		try {
			facturaServices.create(factura);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private void testGetAll()
	{
		System.out.println("\n************ Test getAll() **************************\n");
		List<Factura> facturas = null;
		
		try {
			facturas = facturaServices.getAll();
		
			for(Factura factura: facturas)
			{
				System.out.println(factura);
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public List<Factura> testGetBetweenDates(Date desde, Date hasta)
	{
		System.out.println("\n************ Test getBetweenDates() **************************\n");

		List<Factura> facturasByFecha = null;
		try {
			facturasByFecha = facturaServices.getBetweenDates(desde, hasta);
			
			for(Factura factura: facturasByFecha)
			{
				System.out.println(factura);
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return facturasByFecha;
	}
	
	public List<Factura> testGetByCliente()
	{
		System.out.println("\n************ Test getByCliente(cliente) **************************\n");
		
		List<Factura> facturasByCliente = null; 
		try {
			facturasByCliente = facturaServices.getByCliente(clienteServices.read("37655982K"));
			
			for(Factura factura: facturasByCliente)
			{
				System.out.println(factura);
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return facturasByCliente;
	}

}
