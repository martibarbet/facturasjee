package com.pgrsoft.facturacion.backend.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pgrsoft.facturacion.backend.model.Cliente;
import com.pgrsoft.facturacion.backend.services.ClienteServices;
import com.pgrsoft.facturacion.backend.services.impl.ClienteServicesImpl;


@WebServlet("/testcliente")
public class SvTestCliente extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private ClienteServices clienteServices = new ClienteServicesImpl();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		testRead();
		testCreate();
		testGetAll();
		
		response.sendRedirect("index.jsp");
	}
	
	private void testGetAll() {
		System.out.println("\n************ Test getAll() **************************\n");
		
		List<Cliente> clientes = null;
		
		try {
			clientes = clienteServices.getAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		for(Cliente cliente: clientes) {
			System.out.println(cliente);
		}
		
	}

	private void testCreate() {
		
		System.out.println("\n************ Test Create() **************************\n");
		
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 8; i++)
		{
			sb.append((int)(Math.random() * 10));
		}
		
		char letra = (char) Math.floor(Math.random()*(90-65+1)+65);
		
		String CIF = String.valueOf(sb.toString() + letra);
		
		Cliente cliente = new Cliente(CIF, "ABCDEFG", "Calle Falsa, 123", "Ciudad inexistente" , "123456", "Provinica inventada", "Catalunya");
	
		try {
			clienteServices.create(cliente);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("Cliente [" + CIF + "]: " + cliente);
		
	}

	private void testRead() {
		
		System.out.println("\n************ Test Read() **************************\n");
		
		String CIF1 = "37655982K";
		String CIF2 = "37651982P";
		Cliente cliente1 = null;
		Cliente cliente2 = null;
		
		try {
			cliente1 = clienteServices.read(CIF1);
			cliente2 = clienteServices.read(CIF2);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		System.out.println("Cliente [" + CIF1 + "]: " + cliente1);
		System.out.println("Cliente [" + CIF2 + "]: " + cliente2);
	}

}
